import Models.CursorTrail
import org.jnativehook.GlobalScreen
import org.jnativehook.NativeHookException
import org.jnativehook.keyboard.NativeKeyEvent
import org.jnativehook.keyboard.NativeKeyListener
import java.io.File
import java.io.FileInputStream
import java.io.ObjectInputStream
import java.util.ArrayList
import java.util.logging.Level
import java.util.logging.Logger

class MainBot : NativeKeyListener {

    private var escapePressed = false
    private var cursorTrailCollection: ArrayList<CursorTrail> = ArrayList()

    override fun nativeKeyTyped(p0: NativeKeyEvent?) {

    }

    override fun nativeKeyPressed(e: NativeKeyEvent) {
        if (e.keyCode == 1) {
            escapePressed = true
        }
    }

    override fun nativeKeyReleased(p0: NativeKeyEvent?) {

    }

    private fun startTask() {
        val backgroundThread = Thread(this::runTask)

        backgroundThread.isDaemon = true
        backgroundThread.start()
    }

    private fun runTask() {
        var lastTime = System.nanoTime()
        var timer = 0.0
        val mouseHandler = MouseHandler()
        val inputMax = cursorTrailCollection.size - 1
        var currentArrayListItem = 0
        var doneInputs = 0

        println(inputMax)
        println(cursorTrailCollection[inputMax])

        while (true) {
            if (escapePressed || currentArrayListItem > inputMax) {
                break
            }

            val now = System.nanoTime()
            val elapsed = now - lastTime

            if (elapsed > 0) {
                timer += elapsed
                if (timer >= cursorTrailCollection[currentArrayListItem].timeSinceLastMovement) {
                    println(currentArrayListItem)
                    if (cursorTrailCollection[currentArrayListItem].isClick) {
                        mouseHandler.click(cursorTrailCollection[currentArrayListItem])
                    }
                    mouseHandler.moveCursor(cursorTrailCollection[currentArrayListItem])
                    currentArrayListItem++
                    doneInputs++

                    timer = 0.0
                }
                lastTime = now
            }
        }
    }

    fun moveCursorWhileTrue() {

        val fi = FileInputStream(File("myObjects.dat"))
        val oi = ObjectInputStream(fi)

        val listTest = oi.readObject() as ArrayList<CursorTrail>

        cursorTrailCollection = listTest

        fi.close()
        oi.close()
        startTask()
    }
}

fun main(args: Array<String>) {
    val main = MainBot()
    val logger = Logger.getLogger(GlobalScreen::class.java.`package`.name)

    logger.level = Level.WARNING
    logger.useParentHandlers = false

    registerNativeHook()

    GlobalScreen.addNativeKeyListener(main)

    main.moveCursorWhileTrue()
}

private fun registerNativeHook() {
    try {
        GlobalScreen.registerNativeHook()
    } catch (ex: NativeHookException) {
        System.err.println("There was a problem registering the native hook.")
        System.err.println(ex.message)

        System.exit(1)
    }
}