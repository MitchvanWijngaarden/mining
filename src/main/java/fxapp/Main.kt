package fxapp

import Util.Constants
import javafx.application.Application
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.input.MouseEvent
import javafx.scene.layout.Pane
import javafx.scene.paint.Color
import javafx.scene.shape.Rectangle
import javafx.stage.Stage
import javafx.stage.StageStyle


class Main : Application() {

    private val rectangleCollection = mutableListOf<Rectangle>()

    override fun start(primaryStage: Stage) {
        val root = Pane()
        val image = Image("/gamescreen.png")
        val imageView = ImageView()

        primaryStage.isMaximized = true
        primaryStage.initStyle(StageStyle.UNDECORATED)
        imageView.image = image

        root.children.add(imageView)

        populateRectangles()

        for(rectangle in rectangleCollection) {
            root.children.add(rectangle)
        }

        primaryStage.scene = Scene(root, 1920.0, 1080.0)
        primaryStage.show()
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            launch(Main::class.java)
        }
    }

    private fun populateRectangles() {
        val width = Constants.ironOreIngameWidth
        val height = Constants.ironOreIngameWidth

        val ore1 = Rectangle(Constants.oreOneX, Constants.oreOneY, width, height)
        ore1.fill = Color.PURPLE

        ore1.onMouseClicked = EventHandler<MouseEvent> {
            ore1.fill = Color.RED
            startTask(ore1)
        }

        val ore2 = Rectangle(452.0, 523.0, width, height)
        ore2.fill = Color.PURPLE

        ore2.onMouseClicked = EventHandler<MouseEvent> {
            ore2.fill = Color.RED
            startTask(ore2)
        }

        val ore3 = Rectangle(607.0, 368.0, width, height)
        ore3.fill = Color.PURPLE

        ore3.onMouseClicked = EventHandler<MouseEvent> {
            ore3.fill = Color.RED
            startTask(ore3)
        }


        rectangleCollection.addAll(mutableListOf(ore1, ore2, ore3))
    }

    private fun startTask(rectangle: Rectangle) {
        // Create a Runnable
        val task = Runnable { runTask(rectangle) }

        // Run the task in a background thread
        val backgroundThread = Thread(task)
        // Terminate the running thread if the application exits
        backgroundThread.isDaemon = true
        // Start the thread
        backgroundThread.start()
    }

    private fun runTask(rectangle: Rectangle) {
        try {
            Thread.sleep(5000)
            rectangle.fill = Color.PURPLE
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }
}