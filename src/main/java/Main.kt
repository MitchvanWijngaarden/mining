import Models.CursorTrail
import Models.CursorTrailCollection
import Models.InventoryCursorTrail
import org.jnativehook.GlobalScreen
import org.jnativehook.NativeHookException
import org.jnativehook.keyboard.NativeKeyEvent
import org.jnativehook.keyboard.NativeKeyListener
import org.jnativehook.mouse.NativeMouseEvent
import org.jnativehook.mouse.NativeMouseInputListener
import java.util.logging.Level
import java.util.logging.Logger
import java.io.File
import java.io.FileOutputStream
import java.io.ObjectOutputStream
import java.io.ObjectInputStream
import java.io.FileInputStream
import java.util.ArrayList

class Main : NativeMouseInputListener, NativeKeyListener {
    override fun nativeKeyTyped(p0: NativeKeyEvent?) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun nativeKeyPressed(e: NativeKeyEvent) {
        if (e.keyCode == 1) {
            // Save mining sequence
//            val f = FileOutputStream(File("myObjects.dat"))
//            val o = ObjectOutputStream(f)
//            o.writeObject(cursCollectionArrayList)
//
//            o.close()
//            f.close()
//
//            val fi = FileInputStream(File("myObjects.dat"))
//            val oi = ObjectInputStream(fi)
//
//            val listTest = oi.readObject() as ArrayList<CursorTrail>
//
//            for (cursTrail in listTest) {
//                println("X ${cursTrail.x} Y ${cursTrail.y} Time in between ${cursTrail.timeSinceLastMovement}")
//            }
//
//            fi.close()
//            oi.close()
        }
        shiftHolded = e.keyCode == 42
    }

    override fun nativeKeyReleased(p0: NativeKeyEvent?) {

    }

    override fun nativeMouseClicked(e: NativeMouseEvent) {
        val cursCollection = getCursorTrailCollection()
        val cursCollectionArrayList = cursCollection!!.cursorTrailCollection

        val timeForClick = System.nanoTime() - lastTimeStamp

        val cursorTrailWithClick = CursorTrail()
        cursorTrailWithClick.isClick = true
        cursorTrailWithClick.x = e.x
        cursorTrailWithClick.y = e.y
        cursorTrailWithClick.timeSinceLastMovement = timeForClick

        cursCollectionArrayList.add(cursorTrailWithClick)
    }

    override fun nativeMousePressed(e: NativeMouseEvent) {
        //        System.out.println("Mouse Pressed: " + e.getButton());
    }

    override fun nativeMouseReleased(e: NativeMouseEvent) {
        //        System.out.println("Mouse Released: " + e.getButton());
    }

    override fun nativeMouseMoved(e: NativeMouseEvent) {
        if(shiftHolded) {
            println("Moving cursor in inv sequence.")
            val cursorMoveTimeStamp = System.nanoTime()
            val timeSinceLastTimeStamp = cursorMoveTimeStamp - lastTimeStamp
            val cursCollection = getCursorTrailCollection()
            val inventoryCursorTrail = InventoryCursorTrail()

            lastTimeStamp = cursorMoveTimeStamp

            inventoryCursorTrail.x = e.x
            inventoryCursorTrail.y = e.y
            inventoryCursorTrail.timeSinceLastMovement = timeSinceLastTimeStamp

            inventoryCursCollection.addCursorTrailToCursorTrailCollection(inventoryCursorTrail)

        } else {
            val cursorMoveTimeStamp = System.nanoTime()
            val timeSinceLastTimeStamp = cursorMoveTimeStamp - lastTimeStamp
            val cursCollection = getCursorTrailCollection()
            val cursorTrail = CursorTrail()

            lastTimeStamp = cursorMoveTimeStamp

            cursorTrail.x = e.x
            cursorTrail.y = e.y
            cursorTrail.timeSinceLastMovement = timeSinceLastTimeStamp

            cursCollection!!.addCursorTrailToCursorTrailCollection(cursorTrail)
        }
    }

    override fun nativeMouseDragged(e: NativeMouseEvent) {
        //        System.out.println("Mouse Dragged: " + e.getX() + ", " + e.getY());
    }

    private fun getCursorTrailCollection(): CursorTrailCollection? {
        return cursorTrailCollection
    }

    companion object {
        private var cursorTrailCollection: CursorTrailCollection? = null
        private var inventoryCursCollection: InventoryCursorTrailCollection? = null
        private var lastTimeStamp: Long = 0
        private var shiftHolded: Boolean = false

        @JvmStatic
        fun main(args: Array<String>) {
            val main = Main()
            val logger = Logger.getLogger(GlobalScreen::class.java!!.`package`.name)

            cursorTrailCollection = CursorTrailCollection()
            inventoryCursCollection = InventoryCursorTrailCollection()
            lastTimeStamp = System.nanoTime()

            logger.level = Level.WARNING
            logger.useParentHandlers = false

            registerNativeHook()

            // Add the appropriate listeners.
            GlobalScreen.addNativeMouseListener(main)
            GlobalScreen.addNativeKeyListener(main)
            GlobalScreen.addNativeMouseMotionListener(main)
        }

        private fun registerNativeHook() {
            try {
                GlobalScreen.registerNativeHook()
            } catch (ex: NativeHookException) {
                System.err.println("There was a problem registering the native hook.")
                System.err.println(ex.message)

                System.exit(1)
            }
        }
    }
}