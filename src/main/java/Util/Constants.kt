package Util

class Constants {
    companion object {
        const val ironOreIngameWidth = 130.0

        const val oreOneX = 305.0
        const val oreOneY = 369.0

        const val oreTwoX = 452.0
        const val oreTwoY = 523.0

        const val oreThreeX = 607.0
        const val oreThreeY = 368.0
    }
}