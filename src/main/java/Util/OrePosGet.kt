package Util

import org.jnativehook.GlobalScreen
import org.jnativehook.NativeHookException
import org.jnativehook.mouse.NativeMouseEvent
import org.jnativehook.mouse.NativeMouseInputListener
import java.util.logging.Level
import java.util.logging.Logger

class OrePosGet : NativeMouseInputListener {
    var amountClicked = 0
    var oreCount = 0;

    override fun nativeMouseMoved(p0: NativeMouseEvent?) {

    }

    override fun nativeMouseDragged(p0: NativeMouseEvent?) {

    }

    override fun nativeMouseClicked(e: NativeMouseEvent) {
        amountClicked ++
        if ((amountClicked % 2) == 0) {
            oreCount ++
            println("Ore $oreCount bottom right pos: X ${e.x} Y ${e.y}")
        } else {
            println("Ore top left pos:X ${e.x} Y ${e.y}")
        }
    }

    override fun nativeMousePressed(e: NativeMouseEvent) {

    }

    override fun nativeMouseReleased(e: NativeMouseEvent) {

    }


    companion object {

        @JvmStatic
        fun main(args: Array<String>) {

            registerNativeHook()

            val logger = Logger.getLogger(GlobalScreen::class.java!!.getPackage().getName())
            logger.level = Level.WARNING

            logger.useParentHandlers = false

            // Construct the example object.
            val main = OrePosGet()


            // Add the appropriate listeners.
            GlobalScreen.addNativeMouseListener(main)
        }

        private fun registerNativeHook() {
            try {
                GlobalScreen.registerNativeHook()
            } catch (ex: NativeHookException) {
                System.err.println("There was a problem registering the native hook.")
                System.err.println(ex.message)

                System.exit(1)
            }

        }
    }
}