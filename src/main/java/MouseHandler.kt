import Models.CursorTrail
import java.awt.Robot
import java.awt.event.InputEvent
import java.util.Random

class MouseHandler {

    private val robot = Robot()
    private var amountOfMissedClicks = 0

    fun moveCursor(cursTrail: CursorTrail) {
        robot.mouseMove(cursTrail.x, cursTrail.y)
    }

    fun click(cursorTrail: CursorTrail) {
        val color = robot.getPixelColor(cursorTrail.x, cursorTrail.y)
        val colorRed = color.red
        val colorBlue = color.blue

        if(colorRed == 128 && colorBlue == 128) {
            println("Purple, so click")
            robot.mousePress(InputEvent.BUTTON1_MASK)
            robot.mouseRelease(InputEvent.BUTTON1_MASK)
        } else if(colorRed == 255) {
            println("Red, so sleep")
            sleepClick()
            click(cursorTrail)
        } else {
            if(amountOfMissedClicks > 50) {
                System.exit(1)
            }
            amountOfMissedClicks++
            println("Invalid click $amountOfMissedClicks")

            sleepClick()
            click(cursorTrail)
        }
    }

    private fun sleepClick(){
        val random = Random()
        val low = 1238
        val high = 2234
        val timeToSleep = random.nextInt(high-low) + low

        Thread.sleep(timeToSleep.toLong())
    }
}
