package Models

import java.io.Serializable

data class InventoryCursorTrail(
        var x: Int = 0,
        var y: Int = 0,
        var timeSinceLastMovement: Long = 0,
        var isClick: Boolean = false) : Serializable