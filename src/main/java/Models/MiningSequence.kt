package Models

import java.util.ArrayList

class MiningSequence {
    val miningSequenceCollection = mutableListOf<CursorTrailCollection>()

    fun addCursorTrailToCursorTrailCollection(cursorTrailCollection: CursorTrailCollection) {
        this.miningSequenceCollection.add(cursorTrailCollection)
    }
}
