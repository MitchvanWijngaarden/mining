package Models

import java.util.ArrayList

class InventoryCursorTrailCollection {
    val inventoryCursorTrailCollection: ArrayList<InventoryCursorTrail> = ArrayList()

    fun addInventoryCursorTrailToInventoryCursorTrailCollection(cursorTrail: InventoryCursorTrail) {
        this.inventoryCursorTrailCollection.add(cursorTrail)
    }
}
