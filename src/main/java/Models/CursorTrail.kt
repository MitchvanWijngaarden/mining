package Models

import java.io.Serializable

data class CursorTrail(
        var x: Int = 0,
        var y: Int = 0,
        var timeSinceLastMovement: Long = 0,
        var isClick: Boolean = false) : Serializable