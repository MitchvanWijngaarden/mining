package Models

import java.util.ArrayList

class CursorTrailCollection {
    val cursorTrailCollection: ArrayList<CursorTrail> = ArrayList()

    fun addCursorTrailToCursorTrailCollection(cursorTrail: CursorTrail) {
        this.cursorTrailCollection.add(cursorTrail)
    }
}
